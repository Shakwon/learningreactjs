/** @jsx React.DOM */
'use strict';
define([], function () {
	return React.createClass({
        getInitialState: function() {
            return { data: null };
		},
        componentWillMount: function() {
            $.get('http://api.openweathermap.org/data/2.5/weather?q=izhevsk&appid=2de143494c0b295cca9337e1e96b00e0&units=imperial', function(recieviedData) {
                this.setState({ data: recieviedData });
            }.bind(this));
        },
        fahrenheitToCelsius: function(temp) {
            return (temp - 32) * 5 / 9; 
        },
        getIconUrl: function(icon) {
            return 'http://openweathermap.org/img/w/' + icon + '.png';
        },
        render: function() {
            var control;
            
            if (this.state.data != null)
            {
                control = (
                    <form className='form-horizontal'>
                        <div className='form-group'>
                            <label className='col-sm-2 control-label'>{this.state.data.name}</label>
                            <div className='col-sm-10 form-control-static'>
                                <img src={this.getIconUrl(this.state.data.weather[0].icon)} />
                            </div>
                        </div>
                        <div className='form-group'>
                            <label className='col-sm-2 control-label'>Temp:</label>
                            <div className='col-sm-10 form-control-static'>{Math.round(this.fahrenheitToCelsius(this.state.data.main.temp))}</div>
                        </div>
                    </form>
                );
            }
            
            return (<div>{control}</div>);
        }
    });
});