/** @jsx React.DOM */
'use strict';

require.config({
	baseUrl: 'scripts',
	paths: {
		react: 'script/react.min'
	},
	shim: {
		react: {
			exports: 'React'
		}
	},
     map: {
      '*': { 'jquery': 'jquery-private' },
      'jquery-private': { 'jquery': 'jquery' }
    }
});

require(['app', 'weather'], function (App, Weather) {
	// use app here
	React.renderComponent(
		<App />,
		document.getElementById('app')
	);
    React.renderComponent(
		<Weather />,
		document.getElementById('weather')
	);
});