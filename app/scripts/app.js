/** @jsx React.DOM */
'use strict';
define([], function () {
	return React.createClass({
		getInitialState: function() {
			return {
                value1: 1,
                value2: 2,
                calcValue: "Calc"
            };
		},
        handleChangeValue1: function(event) {
            this.setState({
                value1: event.target.value
            });
        },
        handleChangeValue2: function(event) {
            this.setState({
                value2: event.target.value
            });
        },
		calc: function() {
			this.setState({
                calcValue: Number(this.state.value1) + Number(this.state.value2)
            });
		},
		render: function() {
            return (
                React.DOM.div(null, 
                    React.DOM.input( {type:"text", value:this.state.value1, onChange:this.handleChangeValue1} ), " + " ,
                    React.DOM.input( {type:"text", value:this.state.value2, onChange:this.handleChangeValue2} ), " = ",
                    React.DOM.input( {type:"button", value:this.state.calcValue, onClick:this.calc} )
                )
            );
		}
	});
});
