/** @jsx React.DOM */
'use strict';
define([], function () {
	return React.createClass({
        getInitialState: function() {
            return { data: null };
		},
        componentWillMount: function() {
            $.get('http://api.openweathermap.org/data/2.5/weather?q=izhevsk&appid=2de143494c0b295cca9337e1e96b00e0&units=imperial', function(recieviedData) {
                this.setState({ data: recieviedData });
            }.bind(this));
        },
        fahrenheitToCelsius: function(temp) {
            return (temp - 32) * 5 / 9; 
        },
        getIconUrl: function(icon) {
            return 'http://openweathermap.org/img/w/' + icon + '.png';
        },
        render: function() {
            var control;
            
            if (this.state.data != null)
            {
                control = (
                    React.DOM.form( {className:"form-horizontal"}, 
                        React.DOM.div( {className:"form-group"}, 
                            React.DOM.label( {className:"col-sm-2 control-label"}, this.state.data.name),
                            React.DOM.div( {className:"col-sm-10 form-control-static"}, 
                                React.DOM.img( {src:this.getIconUrl(this.state.data.weather[0].icon)} )
                            )
                        ),
                        React.DOM.div( {className:"form-group"}, 
                            React.DOM.label( {className:"col-sm-2 control-label"}, "Temp:"),
                            React.DOM.div( {className:"col-sm-10 form-control-static"}, Math.round(this.fahrenheitToCelsius(this.state.data.main.temp)))
                        )
                    )
                );
            }
            
            return (React.DOM.div(null, control));
        }
    });
});